const mongoose = require("mongoose");
const config = require("./config");
const { nanoid } = require("nanoid");
const User = require("./models/User");
const Article = require("./models/Article");
const Category = require("./models/Category");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin, author] = await User.create(
    {
      email: "user@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "User",
      avatar: "fixtures/user.png",
      id: nanoid(),
      firstName: "Эмиль",
      lastName: "Алимбеков",
    },
    {
      email: "admin@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "admin",
      displayName: "Admin",
      avatar: "fixtures/admin.png",
      id: nanoid(),
      firstName: "Артур",
      lastName: "Артуров",
    },
    {
      email: "author@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "author",
      displayName: "author",
      avatar: "fixtures/user.png",
      id: nanoid(),
      firstName: "Антон",
      lastName: "Антонов",
    }
  );

  const [category1, category2, category3, category4] = await Category.create(
    {
      id: nanoid(),
      title: "Наука",
    },
    {
      id: nanoid(),
      title: "Здоровье",
    },
    {
      id: nanoid(),
      title: "Видео",
    },
    {
      id: nanoid(),
      title: "Жизнь",
    }
  );

  await Article.create(
    {
      id: nanoid(),
      title: "Тайны мертвых хорошо сохранившийся мозг позволяет заглянуть в будущее",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque saepe alias adipisci unde rerum. Sunt corrupti minus reprehenderit ipsa ab doloribus molestiae perferendis suscipit, quam ducimus harum eos aliquid porro dolor esse cum, expedita voluptatum repellendus explicabo sint fugit quia. Fugit labore accusantium earum est exercitationem recusandae consectetur, soluta totam quod molestiae eius sapiente a iusto autem cumque nihil doloremque atque non, quibusdam deserunt consequuntur eum et! Dolore fugit quibusdam aliquid amet commodi enim sed, quae molestias reprehenderit repudiandae molestiae perspiciatis natus animi voluptatem soluta, expedita saepe deserunt corrupti facilis illum placeat culpa? Inventore, eum mollitia! Incidunt, laudantium fuga.",
      dateCreated: new Date().toISOString(),
      dateUpdate: new Date().toISOString(),
      image: "fixtures/admin.png",
      category: category1,
      author: author,
      views: [user._id, admin._id, author._id],
      viewsQuantity: 3
    },
    {
      id: nanoid(),
      title: "8 способов восстановить разрушенные отношения",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque saepe alias adipisci unde rerum. Sunt corrupti minus reprehenderit ipsa ab doloribus molestiae perferendis suscipit, quam ducimus harum eos aliquid porro dolor esse cum, expedita voluptatum repellendus explicabo sint fugit quia. Fugit labore accusantium earum est exercitationem recusandae consectetur, soluta totam quod molestiae eius sapiente a iusto autem cumque nihil doloremque atque non, quibusdam deserunt consequuntur eum et! Dolore fugit quibusdam aliquid amet commodi enim sed, quae molestias reprehenderit repudiandae molestiae perspiciatis natus animi voluptatem soluta, expedita saepe deserunt corrupti facilis illum placeat culpa? Inventore, eum mollitia! Incidunt, laudantium fuga.",
      dateCreated: new Date().toISOString(),
      dateUpdate: new Date().toISOString(),
      image: "fixtures/admin.png",
      category: category2,
      author: author,
      views: [],
    },
    {
      id: nanoid(),
      title: "Орехи макадамия: весь путь от дерева к столу",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque saepe alias adipisci unde rerum. Sunt corrupti minus reprehenderit ipsa ab doloribus molestiae perferendis suscipit, quam ducimus harum eos aliquid porro dolor esse cum, expedita voluptatum repellendus explicabo sint fugit quia. Fugit labore accusantium earum est exercitationem recusandae consectetur, soluta totam quod molestiae eius sapiente a iusto autem cumque nihil doloremque atque non, quibusdam deserunt consequuntur eum et! Dolore fugit quibusdam aliquid amet commodi enim sed, quae molestias reprehenderit repudiandae molestiae perspiciatis natus animi voluptatem soluta, expedita saepe deserunt corrupti facilis illum placeat culpa? Inventore, eum mollitia! Incidunt, laudantium fuga.",
      dateCreated: new Date().toISOString(),
      dateUpdate: new Date().toISOString(),
      image: "fixtures/admin.png",
      category: category3,
      author: author,
      views: [],
    },
    {
      id: nanoid(),
      title: "A single Life: мультфильм о загадочной пластинке",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque saepe alias adipisci unde rerum. Sunt corrupti minus reprehenderit ipsa ab doloribus molestiae perferendis suscipit, quam ducimus harum eos aliquid porro dolor esse cum, expedita voluptatum repellendus explicabo sint fugit quia. Fugit labore accusantium earum est exercitationem recusandae consectetur, soluta totam quod molestiae eius sapiente a iusto autem cumque nihil doloremque atque non, quibusdam deserunt consequuntur eum et! Dolore fugit quibusdam aliquid amet commodi enim sed, quae molestias reprehenderit repudiandae molestiae perspiciatis natus animi voluptatem soluta, expedita saepe deserunt corrupti facilis illum placeat culpa? Inventore, eum mollitia! Incidunt, laudantium fuga.",
      dateCreated: new Date().toISOString(),
      dateUpdate: new Date().toISOString(),
      image: "fixtures/admin.png",
      category: category4,
      author: author,
      views: [],
    }
  );

  await mongoose.connection.close();
};

run().catch((e) => {
  console.error(e);
  process.exit(1);
});
