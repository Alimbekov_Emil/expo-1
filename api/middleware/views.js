const User = require("../models/User");

const views = async (req, res, next) => {
    const token = req.get("Authorization");

    const user = await User.findOne({ token });

    if (!user) {
        req.user = null;

        return next()
    }

    req.user = user._id;

    return next();
};

module.exports = views;
