const mongoose = require("mongoose");

const ArticleSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  description: {
    type: String,
    required: true,
  },
  dateCreated: {
    type: String,
    required: true,
  },
  dateUpdate: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
    required: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  views: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  }],
  viewsQuantity: {
    type: Number,
    default: 0,
    required: true,
  }
});

const Article = mongoose.model("Article", ArticleSchema);

module.exports = Article;
