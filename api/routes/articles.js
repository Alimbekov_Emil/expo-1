const express = require("express");
const Article = require("../models/Article");
const upload = require("../multer").article;
const auth = require("../middleware/auth.js");
const views = require("../middleware/views.js");
const permit = require("../middleware/permit");
const router = express.Router();

router.get("/", async (req, res) => {
  try {
    if (req.body.category) {
      const articles = await Article.find({ category: req.body.category })
        .populate("category")
        .populate("author", "firstName lastName");
      return res.send(articles);
    } else {
      const articles = await Article.find().populate("category").populate("author");
      return res.send(articles);
    }
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.get("/:id", views, async (req, res) => {
  try {
    const article = await Article.findOne({ _id: req.params.id }).populate("category").populate("author")

    if (req.user) {
      const vieweds = article.views.filter(user => user.equals(req.user))
      if (vieweds.length <= 0) article.views.push(req.user)
      article.viewsQuantity = article.views.length
      await article.save()
    }
    return res.send(article);
  } catch (e) {
    return res.send(e);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    const article = await new Article({
      author: req.user,
      title: req.body.title,
      description: req.body.description,
      image: req.file ? req.file.filename : null,
      dateCreated: new Date().toISOString(),
      dateUpdate: new Date().toISOString(),
      category: req.body.category,
    });
    await article.save();
    return res.send(article);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
  try {
    await Article.deleteOne({ _id: req.params.id });
    return res.send("Your Article deleted");
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;
