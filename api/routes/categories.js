const express = require("express");
const Category = require("../models/Category");
const auth = require("../middleware/auth.js");
const permit = require("../middleware/permit");
const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const category = await Category.find();
    return res.send(category);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post("/", [auth, permit("admin")], async (req, res) => {
  try {
    const category = await new Category({
      title: req.body.title,
    });
    await category.save();
    return res.send(category);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.delete("/", [auth, permit("admin")], async (req, res) => {
  try {
    await Category.deleteOne({ _id: req.params.id });
    return res.send("Your Article deleted");
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;
