import React, {useState} from 'react';
import BottomNavigation from './navigation/BottomNavigation';
import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer} from "@react-navigation/native";
import SoloTask from "./screens/MainScreens/SoloTask";
import {View} from "react-native";
import {AntDesign} from "@expo/vector-icons";
import {blue} from "./constans/colors";
import Login from "./screens/Auth/Login";
import ForgotPassword from "./screens/Auth/ForgotPassword";

export default function App() {
  const Stack = createStackNavigator();
  const [user, setUser] = useState(null )

  return (
    <React.Fragment>
      {
        user ? <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerShown: true,
                headerStyle: {
                  backgroundColor: blue,
                },
                headerTitleAlign: "center",
              }}>
              <Stack.Screen
                name="SIBERS"
              >
                {() => <BottomNavigation setUser={setUser}/>}

              </Stack.Screen>
              <Stack.Screen
                name="TaskId"
                component={SoloTask}
                options={({route, navigation}) => ({
                  title: route.params.title, headerLeft: () => (
                    <View style={{marginHorizontal: 20}}>
                      <AntDesign name="leftcircleo" size={24} color="black" onPress={() => navigation.goBack()}/>
                    </View>
                  ),
                })}
              />
            </Stack.Navigator>
          </NavigationContainer>
          : (
            <NavigationContainer>
              <Stack.Navigator
                screenOptions={{
                  headerShown: true,
                  headerStyle: {
                    backgroundColor: blue,
                  },
                  headerTitleAlign: "center",
                }}>
                <Stack.Screen
                  name="Login"
                >
                  {(props) => <Login {...props} setUser={setUser}/>}
                </Stack.Screen>
                <Stack.Screen
                  name="Forgot"
                  component={ForgotPassword}
                  options={({navigation}) => ({
                    title: 'Forgot', headerLeft: () => (
                      <View style={{marginHorizontal: 20}}>
                        <AntDesign name="leftcircleo" size={24} color="black" onPress={() => navigation.goBack()}/>
                      </View>
                    ),
                  })}
                />
              </Stack.Navigator>
            </NavigationContainer>
          )
      }
    </React.Fragment>
  );
}

