import React from 'react';
import LottieView from 'lottie-react-native';
import {StatusBar, Text, View} from "react-native";
import {blue} from "./constans/colors";

export default class BasicExample extends React.Component {
  componentDidMount() {
    this.animation.play();
    // Or set a specific startFrame and endFrame with:
    this.animation.play(0, 100);
  }

  render() {
    return (
      <View style={{
        position: "absolute",
        top: -150,
        right: 0,
        left: 0,
        bottom: 300,
      }}>
        <View style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: "center",
        }}>
          <LottieView

            ref={animation => {
              this.animation = animation;
            }}
            source={require('./circle.json')}
          />
          <Text style={{
            color: blue
          }}>SIBERS</Text>
        </View>
      </View>
    );
  }
}
