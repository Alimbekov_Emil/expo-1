import React from 'react';
import {Image, Text, TouchableOpacity, View, StyleSheet} from "react-native";
import {BASE_URL} from "../../constans/config";
import moment from "moment";
import {white} from "../../constans/colors";
import {MaterialCommunityIcons} from "@expo/vector-icons";

const Card = ({article}) => {
  return (
    <TouchableOpacity style={styles.card} activeOpacity={1}>
      <View>
        <Image source={{uri: `${BASE_URL}${article.image}`}} style={styles.image}/>
        <Text style={styles.views}>
          <MaterialCommunityIcons style={{marginHorizontal: 5}} name="eye" size={15} color={white}/>
          {article.viewsQuantity}
        </Text>
        <Text style={styles.date}>{moment(article.dateCreated).format('L')}</Text>
        <Text style={styles.text}>
          {article.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    marginVertical: 5,
    width: "44%",
    backgroundColor: white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    padding: 10,
  },
  text: {
    fontSize: 10
  },
  views: {
    position: "absolute", bottom: "30%", right: 10, color: white
  },
  date: {
    position: "absolute", bottom: "22%", right: 10, color: white
  },
  image: {width: "100%", height: 200, position: 'relative'}
})

export default Card;
