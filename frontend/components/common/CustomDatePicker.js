import React from 'react';
import {Dimensions, SafeAreaView, StyleSheet, View} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {blue} from "../../constans/colors";

const CustomDatePicker = ({date, setDate}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <DatePicker
          style={styles.datePickerStyle}
          date={date}
          mode="date"
          format="DD-MM-YYYY"
          minDate="01-01-2016"
          maxDate={new Date()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={(date) => {
            setDate(date);
          }}
        />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  datePickerStyle: {
    borderWidth: 3,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    width: Dimensions.get('window').width - 70,
  },
});

export default CustomDatePicker;


