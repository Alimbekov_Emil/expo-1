import React from "react";
import {Text, View, StyleSheet} from "react-native";
import {TouchableOpacity} from "react-native-gesture-handler";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {blue, white} from "../../constans/colors";

const Link = ({onPresses, children, iconName, iconColor, iconSize, value}) => {
  return (
    <TouchableOpacity onPress={onPresses}>
      <View style={styles.link}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <MaterialCommunityIcons style={{marginHorizontal: 10}} name={iconName} size={iconSize} color={iconColor}/>
          <Text style={{fontSize: 16}}>
            {children}
          </Text>
        </View>
        {value && <Text style={{
          paddingHorizontal: 10, fontSize: 16, color: "#757575"
        }}>{value}</Text>}
      </View>
    </TouchableOpacity>
  )
}


const styles = StyleSheet.create({
  link: {
    flexDirection: "row",
    justifyContent: 'space-between',
    paddingVertical: 10,
    alignItems: 'center',
    marginVertical: 0.5,
    backgroundColor: blue,
    borderBottomWidth: 1,
    borderColor: white,
    borderStyle: "solid",
  }
})

export default Link
