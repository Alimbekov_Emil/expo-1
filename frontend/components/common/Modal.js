import React from 'react';
import {Alert, Dimensions, Modal, Pressable, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View} from 'react-native';
import Title from "./Title";
import {blue, white} from "../../constans/colors";


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const CustomModal = ({setModalVisible, modalVisible, title = 'Add new Task', children, navigation, name=''}) => {
  console.log(name)
  return (
    <SafeAreaView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Title>{title}</Title>
            <ScrollView

              style={{
                paddingBottom: 40
              }}>
              {children}
            </ScrollView>
            <View style={{
              flexDirection: "row",
              alignItems: "center",
            }
            }>
              <Pressable
                style={[styles.button]}
                onPress={() => {
                  setModalVisible(!modalVisible)
                  navigation.navigate('TaskId', {title: name})
                }}>
                <Text style={styles.textStyle}>Save</Text>
              </Pressable>
              <Pressable
                style={[styles.button]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyle}>Cancel</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
  },
  modalView: {
    backgroundColor: 'white',
    padding: 20,
    width: windowWidth,
    height: windowHeight + StatusBar.currentHeight,
    alignItems: 'center',
    shadowColor: '#000',
  },
  button: {
    borderRadius: 20,
    elevation: 2,
    paddingHorizontal: 50,
    paddingVertical: 15,
    marginHorizontal: 10,
    backgroundColor: blue,
  },
  textStyle: {
    color: white,
    fontSize: 17,
  }
});

export default CustomModal;
