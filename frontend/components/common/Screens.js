import React, {useState} from "react";
import {RefreshControl, SafeAreaView, ScrollView, StyleSheet} from "react-native";
import {black, blue} from "../../constans/colors";
import axiosApi from "../../axios";
import BasicExample from "../../BasicExample";

const Screen = ({children, request, setState, prev = false}) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [error, setError] = React.useState('')
  const [loading, setLoading] = useState(false)

  const onRefresh = React.useCallback(async () => {
    if (setState && request) {
      const effect = async () => {
        setLoading(true)
        setRefreshing(true);
        try {
          const response = await axiosApi.get(request)
          if (prev) {
            setState(prevState => (
              [
                ...prevState,
                ...response.data
              ]
            ))
          } else {
            setState(response.data)
          }
          setError(prev => prev + 1)
        } catch (e) {
          setError(e)
        } finally {
          setRefreshing(false);
        }
      }
      effect().then(r => r)

      setTimeout(() => {
        setLoading(false)

      }, 3000)
    }
  }, [request]);

  if (loading) {
    return <BasicExample/>
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        style={styles.screen}>
        <ScrollView style={styles.scrollView}>
          {children}
        </ScrollView>
      </ScrollView>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  scrollView: {
    flex: 1,
  },
  container: {
    flex: 1,
    borderWidth: 1,
    borderColor: blue,
    marginHorizontal: 5,
    borderStyle: "solid"
  },
});

export default Screen;
