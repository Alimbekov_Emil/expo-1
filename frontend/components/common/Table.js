import * as React from 'react';
import {Button, View, Text} from "react-native";
import {blue} from "../../constans/colors";

const CustomTable = ({data, columns}) => {
  const [page, setPage] = React.useState(1);
  const perPage = 10
  const dataTable = React.useMemo(() => {
    if (page === 1) {
      return data.slice(0, perPage - data.length)
    } else {
      return data.slice((page - 1) * perPage, page * perPage)
    }
  }, [data, page])

  const columnsTable = React.useMemo(() => {
    return columns
  }, [columns])

  return (
    <View style={{flex: 1, flexDirection: "column", paddingHorizontal: 5, margin: 5}}>
      <View style={{
        flex: 1,
        flexWrap: 'wrap',
        alignItems: "center",
        flexDirection: "row",
        borderBottom: 1,
        borderColor: "black",
        borderStyle: "solid",
        marginBottom: 5
      }}>
        {
          columnsTable.map((item, index) => {
            return (
              <Text style={{width: item.width}} key={index}>{item.header}</Text>
            )
          })
        }
      </View>

      <View>
        {
          dataTable.map((item, index) => {
            const keys = Object.keys(item)
            return (
              <View key={index} style={{
                flex: 1,
                flexWrap: 'wrap',
                alignItems: "center",
                flexDirection: "row",
                borderBottom: 1,
                borderColor: "black",
                borderStyle: "solid",

                padding: 3,
                marginBottom: 5
              }}>
                {keys.map((key, idx) => {
                  return (
                    <Text key={key} style={{width: idx ? '75%' : '25%', paddingVertical: 5,}}>{item[key]}</Text>
                  )
                })}
              </View>
            )
          })
        }
      </View>
      <View style={{flex: 1, alignItems: "center", flexDirection: "row", flexWrap: "wrap", marginTop: "auto"}}>
        <Button
          title="<"
          color={blue}
          style={{width: "45%"}}
          onPress={() => setPage(prev => prev - 1 <= 0 ? 1 : prev - 1)}
          accessibilityLabel="Learn more about this purple button"/>
        <Button
          title=">"
          color={blue}
          style={{width: "45%"}}
          onPress={() => setPage(prev => prev + 1 <= Math.ceil(data.length / perPage) ? prev + 1 : prev)}
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    </View>
  );
}

export default CustomTable;
