import React from 'react';
import {StyleSheet, Text} from "react-native";

const Title = ({children}) => {
  return (
    <Text style={styles.title}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 20,
  }
})

export default Title;
