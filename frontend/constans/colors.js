export const blue = '#8296e3'
export const grey = '#f4f4f4'
export const darkGrey = '#ebebeb'
export const white = '#ffffff'
export const black = '#000000'
export const darkBlue = '#00267f'
export const red = '#e04d4d'

