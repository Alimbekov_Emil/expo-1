import {blue} from "./colors";

export const inputStyles = {
  borderWidth: 3,
  borderColor: blue,
  borderStyle: "solid",
  borderRadius: 10,
  height: 50,
  paddingHorizontal: 10,
  marginBottom: 15,
}

export const border ={
  borderWidth: 2,
  borderColor: blue,
  borderStyle: "solid",
  borderRadius: 10,
}
