import React from "react";

import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {AntDesign, Entypo, MaterialCommunityIcons} from "@expo/vector-icons";
import Hours from "../screens/MainScreens/Hours";
import Tasks from "../screens/MainScreens/Tasks";
import Contacts from "../screens/MainScreens/Contacts";
import Dayoff from "../screens/MainScreens/Dayoff";
import {black, blue, white} from "../constans/colors";
import More from "../screens/MainScreens/More";

const Tab = createBottomTabNavigator();

const BottomNavigation = () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: blue,
      // inactiveTintColor: white,
      // inactiveBackgroundColor: black,
      // activeBackgroundColor: black,
    }}
    screenOptions={{
      tabBarStyle: {padding: 15},
    }}
  >
    <Tab.Screen
      name="Hours"
      component={Hours}
      options={{
        tabBarIcon: ({size, color}) => <AntDesign style={{paddingTop: 5}} name="clockcircleo" size={size} color={color}/>,
      }}
    />
    <Tab.Screen
      name="Tasks"
      component={Tasks}
      options={{
        tabBarIcon: ({size, color}) => <MaterialCommunityIcons style={{paddingTop: 5}} name="book" size={size} color={color}/>,
      }}
    />
    <Tab.Screen
      name="Contacts"
      component={Contacts}
      options={{
        tabBarIcon: ({size, color}) => <AntDesign style={{paddingTop: 5}} name="contacts" size={size} color={color}/>,
      }}
    />
    <Tab.Screen
      name="Dayoff"
      component={Dayoff}
      options={{
        tabBarIcon: ({size, color}) => <Entypo style={{paddingTop: 5}} name="drink" size={size} color={color}/>,
      }}
    />
    <Tab.Screen
      name="More"
      component={More}
      options={{
        tabBarIcon: ({size, color}) => <AntDesign style={{paddingTop: 5}} name="eyeo" size={size} color={color}/>,
      }}
    />
  </Tab.Navigator>

);

export default BottomNavigation
