import React from 'react';
import {Button, StatusBar, StyleSheet, Text, TextInput, View} from "react-native";
import {blue} from "../../constans/colors";
import Title from "../../components/common/Title";

const ForgotPassword = () => {
  return (
    <View style={styles.container}>
      <Title>Recover Password</Title>
      <Text>Login:</Text>
      <TextInput style={styles.input}/>
      <Button title={'Recover'} color={blue}/>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    marginHorizontal: 25,
  },
  input: {
    marginVertical: 20,
    borderWidth: 3,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    height: 50,
    fontSize: 25
  },
})
export default ForgotPassword;
