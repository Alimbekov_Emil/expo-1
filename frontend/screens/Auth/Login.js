import React from 'react';
import {Button, StatusBar, StyleSheet, Text, TextInput, View} from "react-native";
import {blue} from "../../constans/colors";
import BasicExample from "../../BasicExample";

const Login = ({navigation, setUser}) => {
  return (
    <View style={styles.container}>
      <BasicExample/>
      <Text>User Name</Text>
      <TextInput style={styles.input}/>
      <Text>Password</Text>
      <TextInput style={styles.input}/>
      <Button title={'login'} color={blue} onPress={() => setUser(true)}/>
      <View style={{marginVertical: 10}}>
        <Button title={'Forgot your password?'} color={blue} onPress={() => navigation.navigate('Forgot')}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    marginHorizontal: 25,
    marginTop: 140,
    justifyContent: "center",
  },
  input: {
    marginVertical: 20,
    borderWidth: 3,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    height: 50,
    fontSize: 25
  },
})

export default Login;
