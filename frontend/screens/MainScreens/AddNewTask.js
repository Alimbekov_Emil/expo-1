import React, {useEffect, useState} from 'react';
import CustomModal from "../../components/common/Modal";
import {Dimensions, ScrollView, StatusBar, StyleSheet, Text, TextInput, View} from "react-native";
import {blue} from "../../constans/colors";
import {Picker} from "@react-native-picker/picker";
import CustomDatePicker from "../../components/common/CustomDatePicker";
import axiosApi from "../../axios";
import {inputStyles} from "../../constans";

const AddNewTask = ({modalVisible, setModalVisible, navigation}) => {
  const [phase, setPhase] = useState([
    {title: "Phases"},
  ])
  const [taskLink, setTaskLink] = useState('')
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [from, setFrom] = useState(new Date())
  const [to, setTo] = useState(new Date())
  const [estimation, setEstimation] = useState('')
  const [assigned, setAssigned] = useState([
    {name: "Emma Rose"},
  ])

  const [selectPhase, setSelectPhase] = useState('')
  const [selectAssigned, setSelectAssigned] = useState('')


  useEffect(() => {
    const effect = async () => {
      const response = await axiosApi.get('posts')
      setPhase(prevState => (
        [
          ...prevState,
          ...response.data
        ]
      ))
    }
    effect().then(r => r)
  }, [])

  useEffect(() => {
    const effect = async () => {
      const response = await axiosApi.get('users')
      setAssigned(prevState => (
        [
          ...prevState,
          ...response.data
        ]
      ))
    }
    effect().then(r => r)
  }, []);

  return (
    <CustomModal modalVisible={modalVisible} setModalVisible={setModalVisible} navigation={navigation} name={name}>
      <ScrollView style={[styles.container, {
        marginTop: StatusBar.currentHeight,
        width: Dimensions.get('window').width - 50,
      }]}>
        <Text>Phase</Text>
        <View
          style={
            styles.picker
          }>
          <Picker
            selectedValue={selectPhase}
            onValueChange={(itemValue) => setSelectPhase(itemValue)}
          >
            {
              phase.map((item, index) => (
                <Picker.Item label={item.title} value={item.title} key={index}/>
              ))
            }
          </Picker>
        </View>
        <Text>Task manager link</Text>

        <TextInput value={taskLink} onChange={setTaskLink} style={[styles.input, styles.inputStyles]}/>
        <Text>Name</Text>
        <TextInput value={name} editable onChangeText={(text) => setName(text)} style={[styles.input, styles.inputStyles]}/>

        <Text>Description</Text>
        <TextInput
          multiline
          numberOfLines={10}
          onChangeText={text => setDescription(text)}
          value={description}
          style={[styles.input, styles.inputStyles]}
          editable
        />

        <Text>From</Text>

        <CustomDatePicker date={from} setDate={setFrom}/>
        <Text>To</Text>

        <CustomDatePicker date={to} setDate={setTo}/>
        <Text>Estimation time</Text>

        <TextInput value={estimation} onChange={setEstimation} keyboardType="numeric"
                   style={[styles.input, styles.inputStyles]}/>

        <Text>Assigned to</Text>
        <View
          style={

            styles.picker

          }>
          <Picker
            selectedValue={selectAssigned}
            onValueChange={(itemValue) => setSelectAssigned(itemValue)}
          >
            {
              assigned.map((item, index) => (
                <Picker.Item label={item.name} value={item.name} key={index}/>
              ))
            }
          </Picker>
        </View>
      </ScrollView>
    </CustomModal>
  )
    ;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    height: Dimensions.get('window').height - 130,
  },
  input: {
    margin: 10,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },
  hours: {
    borderWidth: 2,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    width: 60,
    height: 55,
    fontSize: 24,
    paddingLeft: 20
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 22,
    paddingHorizontal: 30,
    borderRadius: 20,
    elevation: 3,
    backgroundColor: blue,
  },
  buttonBlock: {
    paddingTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
  },
  inputStyles, picker: {
    borderWidth: 3,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    height: 50,
    marginHorizontal: 10,
    marginBottom: 15,
    marginTop: 10,
  }
});

export default AddNewTask;
