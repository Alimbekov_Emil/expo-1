import React, {useState, useEffect} from "react";
import {TextInput, View, SafeAreaView, StyleSheet, TouchableOpacity, Text} from "react-native";
import axiosApi from "../../axios";
import Screen from "../../components/common/Screens";
import {white} from "../../constans/colors";

const Contacts = () => {
  const [articles, setArticles] = useState([]);
  const [text, onChangeText] = useState("");


  useEffect(() => {
    const effect = async () => {
      const response = await axiosApi.get('users')
      setArticles(response.data)
    }
    effect().then(r => r)
  }, []);

  return (
    <Screen request={`users?_limit=${text}`} setState={setArticles}>
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={onChangeText}
          value={text}
        />
      </SafeAreaView>
      <View style={{flexDirection: 'row', flexWrap: "wrap", justifyContent: 'space-around'}}>
        {articles.map(user => (
          <TouchableOpacity style={styles.card} activeOpacity={1} key={user.id}>
            <View>
              <Text>Name: {user.name}</Text>
              <Text>Email: {user.email}</Text>
              <Text>Address: {user?.address?.street}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </Screen>
  )
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: white,
    borderRadius: 20
  },
  card: {
    marginVertical: 5,
    width: "45%",
    backgroundColor: white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    padding: 10,
  },
});


export default Contacts
