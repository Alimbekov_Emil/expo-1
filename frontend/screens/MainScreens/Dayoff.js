import React from "react";
import {View, Text} from "react-native";
import Screen from "../../components/common/Screens";

const Dayoff = () => {
  return (
    <Screen>
      <View style={{flexDirection: 'row', flexWrap: "wrap", justifyContent: 'space-around'}}>
        <Text>Dayoff</Text>
      </View>
    </Screen>
  )
}


export default Dayoff
