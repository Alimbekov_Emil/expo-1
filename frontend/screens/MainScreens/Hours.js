import React, {useEffect, useState} from "react";
import {Button, StyleSheet, View} from "react-native";
import Screen from "../../components/common/Screens";
import Title from "../../components/common/Title";
import {blue} from "../../constans/colors";
import axiosApi from "../../axios";
import {Picker} from '@react-native-picker/picker'

const Hours = ({navigation}) => {
  const [tasks, setTasks] = useState([
    {name: "Phases"},
  ])
  const [time, setTime] = useState('')

  useEffect(() => {
    const secTimer = setInterval(() => {
      setTime(new Date().toLocaleString())
    }, 1000)

    return () => clearInterval(secTimer);
  }, []);

  useEffect(() => {
    const effect = async () => {
      const response = await axiosApi.get('users')
      setTasks(prevState => (
        [
          ...prevState,
          ...response.data
        ]
      ))
    }
    effect().then(r => r)
  }, []);


  const [inputs, setInputs] = useState({
    1: '',
    2: '',
    3: '',
    4: "",
    5: ''
  })

  const changeHandler = (value, index) => {
    setInputs(prevState => (
      {
        ...prevState,
        [index]: value
      }
    ))
    value !== 'Phases' && navigation.navigate('TaskId', {title: value})
  }

  const addRow = () => {
    const index = Object.keys(inputs).length + 1
    setInputs(prevState => (
      {
        ...prevState,
        [index]: ''
      }
    ))
  }

  return (
    <Screen request="users" setState={setTasks} prev>
      <View style={styles.container}>
        <Title>{time}</Title>
        {
          Object.keys(inputs).map(item => (
            <View key={item} style={styles.input}>
              <Picker
                selectedValue={inputs[item]}
                style={{
                  width: inputs[item] ? 290 : 320,
                }}
                onValueChange={(itemValue) => changeHandler(itemValue, item)}
              >
                {
                  tasks.map((item, index) => (
                    <Picker.Item label={item.name} value={item.name} key={index}/>
                  ))
                }
              </Picker>

              {inputs[item] && inputs[item] !== 'Phases' ? <Button title="Edit" color={blue} onPress={() =>
                navigation.navigate('TaskId', {title: inputs[item]})
              }/> : null}
            </View>
          ))
        }
        <View style={styles.buttonBlock}>
          <Button title="Add Row" color={blue} onPress={addRow} style={{marginVertical: 20}}/>
          <Button title="Submit" color={blue} style={{marginVertical: 20}}/>
        </View>
      </View>
    </Screen>

  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 40,
  },
  input: {
    flex: 1,
    marginTop: 10,
    alignItems: "center",
    flexDirection: "row",
    borderWidth: 3,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10
  },
  buttonBlock: {
    paddingTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
export default Hours
