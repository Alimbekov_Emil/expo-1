import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import MainSetting from "../settingScreens/MainSetting";
import {black, darkBlue, white} from '../../constans/colors'
import Projects from '../settingScreens/Projects'
import Karma from '../settingScreens/Karma'
import Documents from '../settingScreens/Documents'
import Events from '../settingScreens/Events'


const Stack = createStackNavigator();

const More = () => (
  <Stack.Navigator
    screenOptions={{
      headerTintColor: black,
      headerTitleStyle: {
        fontSize: 24,
        color:white
      },
      headerStyle: {backgroundColor: darkBlue,},
      headerLeftContainerStyle:{
        color:white
      }
    }}
  >
    <Stack.Screen
      name="More"
      component={MainSetting}
      options={{headerTitle: "More"}}
    />
    <Stack.Screen
      name="Karma"
      component={Karma}
      options={{headerTitle: "Karma"}}
    />
    <Stack.Screen
      name="Events"
      component={Events}
      options={{headerTitle: "Перейти к профилю"}}
    />
    <Stack.Screen
      name="Documents"
      component={Documents}
      options={{headerTitle: "Стать Автором"}}
    />
    <Stack.Screen
      name="Projects"
      component={Projects}
      options={{headerTitle: "Настроить интерфейс"}}

    />
  </Stack.Navigator>
);


export default More
