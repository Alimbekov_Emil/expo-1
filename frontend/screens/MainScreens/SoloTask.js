import React, {useEffect, useState} from 'react';
import {Button, Dimensions, Pressable, ScrollView, StyleSheet, Text, TextInput, View} from "react-native";
import Screen from "../../components/common/Screens";
import {blue, red, white} from "../../constans/colors";
import axiosApi from "../../axios";
import {Picker} from '@react-native-picker/picker'
import AddNewTask from "./AddNewTask";
import {border} from "../../constans";

const SoloTask = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const progress = ['In progress', 'Done', 'Reject']
  const [tasks, setTasks] = useState([])
  const [input, setInput] = useState('');
  const [hours, setHours] = useState('');
  const [comment, setComment] = useState('')
  const [status, setStatus] = useState('in progress')

  useEffect(() => {
    const effect = async () => {
      const response = await axiosApi.get('posts')
      setTasks(prevState => (
        [
          ...prevState,
          ...response.data
        ]
      ))
    }
    effect().then(r => r)
  }, []);

  return (
    <Screen request={'posts'} setState={setTasks} prev>
      <ScrollView style={styles.container}>
        <Text>Tasks</Text>
        <View style={styles.input}>
          <View style={styles.border}>
            <Picker
              selectedValue={input}
              style={{
                width: Dimensions.get('window').width - 150,
                height: 50
              }}
              onValueChange={(itemValue) => setInput(itemValue)}
            >
              {
                tasks.map((item, index) => (
                  <Picker.Item label={item.title} value={item.title} key={index}/>
                ))
              }
            </Picker>
          </View>
          <TextInput value={hours} onChange={setHours} style={styles.hours} keyboardType="numeric"/>
          <Text style={{position: "absolute", top: -25, right: 30}}>Hours</Text>
        </View>
        <Text>Status</Text>
        <View style={styles.input}>
          <View style={styles.border}>
            <Picker
              selectedValue={status}
              onValueChange={(itemValue) => setStatus(itemValue)}
              style={{
                width: Dimensions.get('window').width - 65,
                height: 50
              }}
            >
              {
                progress.map((item, index) => (
                  <Picker.Item label={item} value={item} key={index}/>
                ))
              }
            </Picker>
          </View>
        </View>

        <Text>Comments</Text>
        <View
          style={[{margin: 10,}, styles.border]}>
          <TextInput
            multiline
            numberOfLines={10}
            onChangeText={text => setComment(text)}
            value={comment}
            style={{padding: 10}}
            editable
          />
        </View>
        <View style={styles.buttonBlock}>
          <Button title="Add New Task" color={blue} style={{marginVertical: 20}} onPress={() => setModalVisible(!modalVisible)}/>
          <Button title="Delete" color={red} style={{marginVertical: 20}}/>
        </View>
      </ScrollView>
      <Pressable style={styles.button} onPressIn={() =>
        navigation.navigate('Hours')
      }>
        <Text style={{color: white}}>
          Save
        </Text>
      </Pressable>
      <AddNewTask setModalVisible={setModalVisible} modalVisible={modalVisible} navigation={navigation}/>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    height: Dimensions.get('window').height - 130,
  },
  input: {
    margin: 10,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },
  hours: {
    borderWidth: 2,
    borderColor: blue,
    borderStyle: "solid",
    borderRadius: 10,
    width: 60,
    height: 55,
    fontSize: 24,
    paddingLeft: 20
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 22,
    paddingHorizontal: 30,
    borderRadius: 20,
    elevation: 3,
    backgroundColor: blue,
  },
  buttonBlock: {
    paddingTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
  },
  border
});

export default SoloTask;
