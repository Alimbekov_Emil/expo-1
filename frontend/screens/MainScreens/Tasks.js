import React, {useState} from "react";
import {Button, Dimensions, ScrollView, StyleSheet, Text} from "react-native";
import Screen from "../../components/common/Screens";
import Title from "../../components/common/Title";
import AddNewTask from "./AddNewTask";
import {blue} from "../../constans/colors";


const Tasks = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [tasks, setTasks] = useState([])

  return (<Screen request={'todo?_limit=3'} setState={setTasks} prev>
    <ScrollView style={styles.container}>
      <Title>Tasks</Title>

      <Button title='Create Task' color={blue} onPress={() => setModalVisible(!modalVisible)}/>
    </ScrollView>

    <AddNewTask setModalVisible={setModalVisible} modalVisible={modalVisible} navigation={navigation}/>

  </Screen>)
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    height: Dimensions.get('window').height - 130,
  },
})

export default Tasks
