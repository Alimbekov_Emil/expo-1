import * as React from 'react';
import {Searchbar} from 'react-native-paper';
import CustomTable from "../../components/common/Table";
import {View} from "react-native";
import Screen from "../../components/common/Screens";

const columns = [
  {
    header: "Date",
    accessor: "date",
    width: "24%"
  },
  {
    header: "Title",
    accessor: "title",
    width: "74%"
  },
  // {
  //   header: "Description",
  //   accessor: "description"
  // }
]

const data = [
  {
    date: '1',
    title: "22 Февраля: Мужской Вторник",
  },
  {
    date: '2',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '3',
    title: "22 Февраля: Мужской Вторник",
  }
  , {
    date: '4',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '5',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '6',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '7',
    title: "22 Февраля: Мужской Вторник",
  },
  {
    date: '8',
    title: "22 Февраля: Мужской Вторник",
  },
  {
    date: '9',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '10',
    title: "22 Февраля: Мужской Вторник",
  }
  , {
    date: '11',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '12',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '13',
    title: "22 Февраля: Мужской Вторник",
  }, {
    date: '14',
    title: "22 Февраля: Мужской Вторник",
  }
]
const Events = ({navigation}) => {
  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  return (
    <Screen>
      <Searchbar
        placeholder="Search"
        onChangeText={onChangeSearch}
        value={searchQuery}
        onIconPress={() => setSearchQuery('asd')}
        style={{padding: 0}}
      />

      <View>
        <CustomTable columns={columns} data={data}/>
      </View>
    </Screen>
  );
}


export default Events
