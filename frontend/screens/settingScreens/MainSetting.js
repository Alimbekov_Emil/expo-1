import React from "react";
import {View, StyleSheet, Text} from "react-native";
import Link from "../../components/common/Link";
import Screen from "../../components/common/Screens";
import {black, blue} from "../../constans/colors";


const MainSetting = ({navigation}) => {
  return (
    <Screen>
      <View style={styles.setting}>
        <Link iconColor={black} iconSize={25} iconName="email"
              onPresses={() => navigation.navigate('Karma')}><Text>Karma</Text></Link>
        <Link iconColor={black} iconSize={25} iconName="lamp" onPresses={() => navigation.navigate('Events')}><Text>Events</Text></Link>
        <Link iconColor={black} iconSize={25} iconName="lamp"
              onPresses={() => navigation.navigate('Documents')}>Documents</Link>
        <Link iconColor={black} iconSize={25} iconName="lamp"
              onPresses={() => navigation.navigate('Projects')}><Text>Projects</Text></Link>
      </View>
    </Screen>
  )
}


const styles = StyleSheet.create({
  setting: {
    paddingVertical: 20
  },
  text: {
    color: "#757575",
    padding: 10,
    textTransform: 'uppercase'
  },
  link: {
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: 'space-between',
    padding: 15,
    alignItems: 'center',
    marginVertical: 0.5,
    textTransform: 'uppercase',
    fontWeight: "700",
    color: blue
  }
})


export default MainSetting
